// Mock database
// let posts = [];

// The "fetch" method will return a "promise" that resolves to "response" object
fetch('https://jsonplaceholder.typicode.com/posts')
// The first ".then" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json()) 
// The second ".then" method calls the showPosts() function to print the converted JSON value from the fetched request
.then((data) => showPosts(data));

	


// Posts ID
let count = 1;

// ADD POST DATA
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// Prevents the page from reloading
	// Prevents default behavior of event
	e.preventDefault();

/*
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// Increment count for post ids, will increment everytime a new post is added
	count++;

	console.log(posts);
	

	// Call the showPosts function to display our posts
	showPosts(posts);
	alert("Post Successfully Added!");
*/
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		// JSON.stringify converts the object data into stringified JSON
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		// Sets the header data of the "request" object to be sent to the backend
		headers: {'Content-type' : 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})

});

// RETRIEVE POSTS
const showPosts = (posts) => {
	let postEntries = "";

	// Create a mock HTML file
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});

	// console.log(postEntries);
	// Insert the postEntries HTML code into the empty div in our HTML
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// EDIT POST
const editPost = (id) => {
	// Get the title and body of the post using the id passed to the editPost function
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Populate the edit form fields
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	// To remove the "disabled" attribute from the update button
	// removeAttributes() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// UPDATE POST
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

/*
	// Use a for loop to match the id of the post to be edited with the post inside of our posts array
	for(let i = 0; i < posts.length; i++){
		if(document.querySelector('#txt-edit-id').value === posts[i].id.toString()){

			// Reassign the title and body of the post in the array to the new title and body
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			// Call showPosts again to update the output
			showPosts(posts);
			alert("Successfully updated!");

			// Use break to end the loop
			break;
		};
	}
*/
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		// Sets the header data of the "request" object to be sent to the backend
		headers: {'Content-type' : 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!')

		// Reset the edit post form input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		// .setAttribute - sets an attribute to an HTML element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
});

// ACTIVITY
// DELETE POST
const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	});

	// The element.remove() method removes the element from the DOM
	// document.querySelector(`#post-${id}`).remove();
	// alert("Successfully deleted post!");
};